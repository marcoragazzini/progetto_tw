-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3307
-- Creato il: Gen 02, 2021 alle 17:24
-- Versione del server: 10.4.14-MariaDB
-- Versione PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_tw`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `carrello`
--

CREATE TABLE `carrello` (
  `Id_Carrello` int(11) NOT NULL,
  `Utente` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `carrello`
--

INSERT INTO `carrello` (`Id_Carrello`, `Utente`) VALUES
(2, 'benedetta.bottari@gmail.it'),
(1, 'marco.ragazzini@gmail.it');

-- --------------------------------------------------------

--
-- Struttura della tabella `carta`
--

CREATE TABLE `carta` (
  `Numero` char(20) NOT NULL,
  `Marca` char(50) NOT NULL,
  `Intestatario` char(50) NOT NULL,
  `Tipologia` char(50) NOT NULL,
  `Scadenza` date NOT NULL,
  `CVV` char(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `carta`
--

INSERT INTO `carta` (`Numero`, `Marca`, `Intestatario`, `Tipologia`, `Scadenza`, `CVV`) VALUES
('3000 0000 0000 0000', 'amex', 'Marco', 'carta di debito', '2023-05-01', '256'),
('3525 2222 2222 2222', 'amex', 'Marco', 'carta di debito', '2030-05-01', '515'),
('5252 5252 0000 0000', 'mastercard', 'Benedetta', 'carta di debito', '2302-06-01', '545');

-- --------------------------------------------------------

--
-- Struttura della tabella `carta_utente`
--

CREATE TABLE `carta_utente` (
  `Utente` char(50) NOT NULL,
  `Carta` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `carta_utente`
--

INSERT INTO `carta_utente` (`Utente`, `Carta`) VALUES
('benedetta.bottari@gmail.it', '5252 5252 0000 0000'),
('marco.ragazzini@gmail.it', '3000 0000 0000 0000'),
('marco.ragazzini@gmail.it', '3525 2222 2222 2222');

-- --------------------------------------------------------

--
-- Struttura della tabella `indirizzo`
--

CREATE TABLE `indirizzo` (
  `Id_Indirizzo` int(11) NOT NULL,
  `Regione` char(50) NOT NULL,
  `Provincia` char(50) NOT NULL,
  `CAP` char(50) NOT NULL,
  `Comune` char(50) NOT NULL,
  `Via_Num` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `indirizzo`
--

INSERT INTO `indirizzo` (`Id_Indirizzo`, `Regione`, `Provincia`, `CAP`, `Comune`, `Via_Num`) VALUES
(1, 'emilia-romagna', 'RA', '48018', 'Faenza', 'Via San Giovannino 45'),
(2, 'emilia-romagna', 'FC', '47521', 'Cesena', ' Via Corte Botticelli 21');

-- --------------------------------------------------------

--
-- Struttura della tabella `notifica`
--

CREATE TABLE `notifica` (
  `Nome` char(50) NOT NULL,
  `Descrizione` char(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `notifica`
--

INSERT INTO `notifica` (`Nome`, `Descrizione`) VALUES
('Benvenuto', 'la registrazione è avvenuta con Successo! Benvenuto nel nostro sito, speriamo che tu ti possa trovare bene'),
('Carta aggiunta', 'La carta è stata aggiunta con successo!'),
('Indirizzo aggiunto', 'L\'indirizzo è stato aggiunto con successo!'),
('Nuovo coupon', 'è ora disponibile, fai un salto alla sezione apposita per maggiori informazioni'),
('Ordine completato', 'Il tuo ordine è stato completato con successo!'),
('Ordine effettuato', 'E\' stato effettuato un nuovo ordine dal nostro Store!'),
('Pagamento è avvenuto con successo', 'L\'ultimo pagamento è avvenuto con successo! Speriamo che continuerai a comprare dal nostro negozio'),
('Prodotto non più disponibile', 'non è più disponibile, quindi lo abbiamo tolto dal suo carrello!');

-- --------------------------------------------------------

--
-- Struttura della tabella `offerta`
--

CREATE TABLE `offerta` (
  `Codice` char(50) NOT NULL,
  `Descrizione` char(250) NOT NULL,
  `Data_Scadenza` date NOT NULL,
  `Venditore` char(50) NOT NULL,
  `Nome` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `offerta`
--

INSERT INTO `offerta` (`Codice`, `Descrizione`, `Data_Scadenza`, `Venditore`, `Nome`) VALUES
('WAN_3X2_1120', 'Prendi 3 prodotti e quello meno costoso non lo paghi', '2021-01-01', 'marco.ragazzini@tecweb.it', 'COUPON 3x2'),
('WAN_C5_1220', 'Ottieni il 5% di sconto sull\'intero ammontare!', '2021-05-01', 'marco.ragazzini@tecweb.it', 'Sconto 5%'),
('WAN_F+S_1120', 'Se spendi almeno 5 euro la Spedizione non la paghi!', '2021-01-01', 'marco.ragazzini@tecweb.it', 'Free Ship');

-- --------------------------------------------------------

--
-- Struttura della tabella `offerta_utente`
--

CREATE TABLE `offerta_utente` (
  `Utente` char(50) NOT NULL,
  `Offerta` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `offerta_utente`
--

INSERT INTO `offerta_utente` (`Utente`, `Offerta`) VALUES
('marco.ragazzini@gmail.it', 'WAN_3X2_1120'),
('marco.ragazzini@gmail.it', 'WAN_C5_1220');

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine`
--

CREATE TABLE `ordine` (
  `Carrello` int(11) NOT NULL,
  `Offerta` char(50) DEFAULT NULL,
  `Carta` char(20) NOT NULL,
  `Indirizzo` int(11) NOT NULL,
  `Eseguito` tinyint(1) NOT NULL DEFAULT 0,
  `Data` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto`
--

CREATE TABLE `prodotto` (
  `Nome` char(50) NOT NULL,
  `Prezzo` decimal(10,2) NOT NULL,
  `Descrizione` char(250) NOT NULL,
  `Immagine` char(50) NOT NULL,
  `Disponibile` char(1) NOT NULL,
  `N_Vendite` int(7) NOT NULL DEFAULT 0,
  `Tipo` char(50) NOT NULL,
  `Venditore` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `prodotto`
--

INSERT INTO `prodotto` (`Nome`, `Prezzo`, `Descrizione`, `Immagine`, `Disponibile`, `N_Vendite`, `Tipo`, `Venditore`) VALUES
('Bacon Eggs', '1.00', 'Descrizione', './assets/img/menu/salato/bacon eggs.jpg', '1', 0, 'salato', 'benedetta.bottari@tecweb.it'),
('bagel farcito', '6.00', 'pasta lievitata polacca, farcita con salmone, avocado e formaggio spalmabile', './assets/img/menu/salato/bagel farcito.jpg', '1', 0, 'salato', 'benedetta.bottari@tecweb.it'),
('Baklava', '1.00', 'Descrizione', './assets/img/menu/torta/baklava.jpg', '1', 0, 'torta', 'benedetta.bottari@tecweb.it'),
('Café au lait', '1.20', 'Caffè con una dose di spuma di latte', './assets/img/menu/bevanda/café au lait.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('Café de olla', '2.30', 'Caffè in polvere, acqua, spezie e un bastoncino di cannella', './assets/img/menu/bevanda/café de olla.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('Caffè americano', '2.00', '1 espresso e 2 dosi d\'acqua', './assets/img/menu/bevanda/caffè americano.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('Caffè turco', '2.20', 'Caffè in polvere in acqua bollente e cardamomo', './assets/img/menu/bevanda/caffè turco.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('Cappuccino', '1.40', 'Caffè espresso con spuma di latte', './assets/img/menu/bevanda/cappuccino.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('Churros', '1.00', 'Descrizione', './assets/img/menu/dolce/churros.jpg', '1', 0, 'dolce', 'benedetta.bottari@tecweb.it'),
('Cimbalino', '1.60', 'Caffè lungo', './assets/img/menu/bevanda/cimbalino.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('Cioccocafè', '3.00', 'Cioccolata in tazza con una dose di espresso', './assets/img/menu/bevanda/cioccocafè.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('club sandwich', '5.50', 'Tramezzino di origine americana, ripieno con pollo, insalata, pomodoro e bacon croccante.', './assets/img/menu/salato/club sandwich.jpg', '1', 0, 'salato', 'benedetta.bottari@tecweb.it'),
('Conchas', '1.00', 'Descrizione', './assets/img/menu/dolce/conchas.jpg', '1', 0, 'dolce', 'benedetta.bottari@tecweb.it'),
('Coradito', '1.00', '1 espresso con schiuma di latte e zucchero', './assets/img/menu/bevanda/coradito.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('Cornetto', '1.00', 'Descrizione', './assets/img/menu/dolce/cornetto.jpg', '1', 0, 'dolce', 'benedetta.bottari@tecweb.it'),
('donuts', '3.00', 'Ciambella , originaria degli Stati Uniti e del Canada, glassata al cioccolato', './assets/img/menu/dolce/donuts.jpg', '1', 0, 'dolce', 'benedetta.bottari@tecweb.it'),
('Dorayaki', '1.00', 'Descrizione', './assets/img/menu/dolce/dorayaki.jpg', '1', 0, 'dolce', 'benedetta.bottari@tecweb.it'),
('Einspanner', '1.80', '2 espressi con l\'aggiunta di panna montata', './assets/img/menu/bevanda/einspanner.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('Eiskaffe', '3.80', 'Caffè freddo con gelato alla vaniglia e panna montata', './assets/img/menu/bevanda/eiskaffe.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('English tea', '2.90', 'Tea con latte', './assets/img/menu/bevanda/english tea.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('Flan di pere', '1.00', 'Descrizione', './assets/img/menu/torta/flan di pere.jpg', '1', 0, 'torta', 'benedetta.bottari@tecweb.it'),
('Frappè', '2.70', 'Ghiaccio immerso in caffè solubile con acqua e latte condensato', './assets/img/menu/bevanda/frappè.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('Funnel cake', '1.00', 'Descrizione', './assets/img/menu/torta/funnel cake.jpg', '1', 0, 'torta', 'benedetta.bottari@tecweb.it'),
('Gaourti kai frouta', '1.00', 'Descrizione', './assets/img/menu/dolce/gaourti kai frouta.jpg', '1', 0, 'dolce', 'benedetta.bottari@tecweb.it'),
('Irish coffe', '3.60', 'Caffè, zucchero di canna, un bicchierino di whisky e panna densa', './assets/img/menu/bevanda/irish coffe.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('Karsk', '2.60', 'Caffè espresso e liquore', './assets/img/menu/bevanda/karsk.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('Koffie verkeerd', '1.90', 'Caffè filtrato e 2 dosi di latte', './assets/img/menu/bevanda/koffie verkeerd.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('Krapfen', '1.00', 'Descrizione', './assets/img/menu/dolce/krapfen.jpg', '1', 0, 'dolce', 'benedetta.bottari@tecweb.it'),
('Lange koffie', '1.30', 'Caffè alto', './assets/img/menu/bevanda/lange koffie.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('Macaron', '1.00', 'Descrizione', './assets/img/menu/dolce/macaron.jpg', '1', 0, 'dolce', 'benedetta.bottari@tecweb.it'),
('Menù austria', '1.00', 'Descrizione', './assets/img/menu/menu/austria.jpg', '1', 0, 'Menù', 'benedetta.bottari@tecweb.it'),
('Menù belgio', '1.00', 'Descrizione', './assets/img/menu/menu/belgio.jpg', '1', 0, 'Menù', 'benedetta.bottari@tecweb.it'),
('Menù cuba', '1.00', 'Descrizione', './assets/img/menu/menu/cuba.jpg', '1', 0, 'Menù', 'benedetta.bottari@tecweb.it'),
('Menù francia', '1.00', 'Descrizione', './assets/img/menu/menu/francia.jpg', '1', 0, 'Menù', 'benedetta.bottari@tecweb.it'),
('Menù germania', '1.00', 'Descrizione', './assets/img/menu/menu/germania.jpg', '1', 0, 'Menù', 'benedetta.bottari@tecweb.it'),
('Menù giappone', '1.00', 'Descrizione', './assets/img/menu/menu/giappone.jpg', '1', 0, 'Menù', 'benedetta.bottari@tecweb.it'),
('Menù grecia', '1.00', 'Descrizione', './assets/img/menu/menu/grecia.jpg', '1', 0, 'Menù', 'benedetta.bottari@tecweb.it'),
('Menù irlanda', '1.00', 'Descrizione', './assets/img/menu/menu/irlanda.jpg', '1', 0, 'Menù', 'benedetta.bottari@tecweb.it'),
('Menù italia', '1.00', 'Descrizione', './assets/img/menu/menu/italia.jpg', '1', 0, 'Menù', 'benedetta.bottari@tecweb.it'),
('Menù messico', '1.00', 'Descrizione', './assets/img/menu/menu/messico.jpg', '1', 0, 'Menù', 'benedetta.bottari@tecweb.it'),
('Menù norvegia', '1.00', 'Descrizione', './assets/img/menu/menu/norvegia.jpg', '1', 0, 'Menù', 'benedetta.bottari@tecweb.it'),
('Menù olanda', '1.00', 'Descrizione', './assets/img/menu/menu/olanda.jpg', '1', 0, 'Menù', 'benedetta.bottari@tecweb.it'),
('Menù portogallo', '1.00', 'Descrizione', './assets/img/menu/menu/portogallo.jpg', '1', 0, 'Menù', 'benedetta.bottari@tecweb.it'),
('Menù regnounito', '1.00', 'Descrizione', './assets/img/menu/menu/regno-unito.jpg', '1', 0, 'Menù', 'benedetta.bottari@tecweb.it'),
('Menù spagna', '1.00', 'Descrizione', './assets/img/menu/menu/spagna.jpg', '1', 0, 'Menù', 'benedetta.bottari@tecweb.it'),
('Menù statiuniti', '1.00', 'Descrizione', './assets/img/menu/menu/stati-uniti.jpg', '1', 0, 'Menù', 'benedetta.bottari@tecweb.it'),
('Menù turchia', '1.00', 'Descrizione', './assets/img/menu/menu/turchia.jpg', '1', 0, 'Menù', 'benedetta.bottari@tecweb.it'),
('Pancake', '1.00', 'Descrizione', './assets/img/menu/dolce/pancake.jpg', '1', 0, 'dolce', 'benedetta.bottari@tecweb.it'),
('pumpkin spice latte', '3.70', 'Caffè ristretto con latte aromatizzato alla zucca, panna montata e cannella', './assets/img/menu/bevande/pumpkin spice latte.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('Sacher', '1.00', 'Descrizione', './assets/img/menu/torta/sacher.jpg', '1', 0, 'torta', 'benedetta.bottari@tecweb.it'),
('Skoleboller', '1.00', 'Descrizione', './assets/img/menu/dolce/skoleboller.jpg', '1', 0, 'dolce', 'benedetta.bottari@tecweb.it'),
('Stroopwafel', '1.00', 'Descrizione', './assets/img/menu/dolce/stroopwafel.jpg', '1', 0, 'dolce', 'benedetta.bottari@tecweb.it'),
('Tea matcha', '2.50', 'Re dei tea verdi o elisir della salute', './assets/img/menu/bevanda/tea matcha.jpg', '1', 0, 'bevanda', 'benedetta.bottari@tecweb.it'),
('Torta Cubana', '1.00', 'Descrizione', './assets/img/menu/torta/torta cubana.jpg', '1', 0, 'torta', 'benedetta.bottari@tecweb.it'),
('Waffle', '1.00', 'Descrizione', './assets/img/menu/dolce/waffle.jpg', '1', 0, 'dolce', 'benedetta.bottari@tecweb.it');

-- --------------------------------------------------------

--
-- Struttura della tabella `prod_carr`
--

CREATE TABLE `prod_carr` (
  `Carrello` int(11) NOT NULL,
  `Prodotto` char(50) NOT NULL,
  `Data` datetime NOT NULL,
  `Quantita` decimal(3,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `prod_carr`
--

INSERT INTO `prod_carr` (`Carrello`, `Prodotto`, `Data`, `Quantita`) VALUES
(1, 'Cimbalino', '2020-12-20 18:01:40', '4'),
(1, 'Coradito', '2020-12-08 16:44:39', '8'),
(2, 'Karsk', '2020-12-08 16:44:39', '7');

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `Email` char(50) NOT NULL,
  `Password` char(250) NOT NULL,
  `Nome` char(50) NOT NULL,
  `Cognome` char(50) NOT NULL,
  `Telefono` char(50) NOT NULL,
  `Sesso` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`Email`, `Password`, `Nome`, `Cognome`, `Telefono`, `Sesso`) VALUES
('benedetta.bottari@gmail.it', '$2y$10$t9rWltOIGv97NbDdmI8/Vu81/xgiTCKVqZPavog1HBePO5qoTr.ji', 'Benedetta', 'Bottari', '123-345-6666', 'F'),
('marco.ragazzini@gmail.it', '$2y$10$wWCAo7Bdzk.lueoiBh.DweNeGkJpM8CfUVvy88D2yVX486eUbJNZi', 'Marco', 'Ragazzini', '123-345-6666', 'M');

-- --------------------------------------------------------

--
-- Struttura della tabella `utente_indirizzo`
--

CREATE TABLE `utente_indirizzo` (
  `Indirizzo` int(11) NOT NULL,
  `Utente` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `utente_indirizzo`
--

INSERT INTO `utente_indirizzo` (`Indirizzo`, `Utente`) VALUES
(1, 'marco.ragazzini@gmail.it'),
(2, 'benedetta.bottari@gmail.it'),
(2, 'marco.ragazzini@gmail.it');

-- --------------------------------------------------------

--
-- Struttura della tabella `utente_notifica`
--

CREATE TABLE `utente_notifica` (
  `Id_UN` int(11) NOT NULL,
  `Notifica` char(50) NOT NULL,
  `Utente` char(50) NOT NULL,
  `Data` datetime NOT NULL,
  `Descrizione` char(250) NOT NULL,
  `Visualizzato` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `utente_notifica`
--

INSERT INTO `utente_notifica` (`Id_UN`, `Notifica`, `Utente`, `Data`, `Descrizione`, `Visualizzato`) VALUES
(1, 'Benvenuto', 'marco.ragazzini@gmail.it', '2021-01-02 17:17:22', 'Marco Ragazzini la registrazione è avvenuta con Successo! Benvenuto nel nostro sito, speriamo che tu ti possa trovare bene', 1),
(2, 'Indirizzo aggiunto', 'marco.ragazzini@gmail.it', '2021-01-02 17:17:22', 'L\'indirizzo è stato aggiunto con successo!', 1),
(3, 'Carta aggiunta', 'marco.ragazzini@gmail.it', '2021-01-02 17:17:22', 'La carta è stata aggiunta con successo!', 1),
(4, 'Benvenuto', 'benedetta.bottari@gmail.it', '2021-01-02 17:20:15', 'Benedetta Bottari la registrazione è avvenuta con Successo! Benvenuto nel nostro sito, speriamo che tu ti possa trovare bene', 1),
(5, 'Indirizzo aggiunto', 'benedetta.bottari@gmail.it', '2021-01-02 17:17:22', 'L\'indirizzo è stato aggiunto con successo!', 1),
(6, 'Carta aggiunta', 'benedetta.bottari@gmail.it', '2021-01-02 17:17:22', 'La carta è stata aggiunta con successo!', 1),
(7, 'Carta aggiunta', 'marco.ragazzini@gmail.it', '2021-01-02 17:17:22', 'La carta è stata aggiunta con successo!', 1),
(8, 'Indirizzo aggiunto', 'marco.ragazzini@gmail.it', '2021-01-02 17:17:22', 'L\'indirizzo è stato aggiunto con successo!', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `venditore`
--

CREATE TABLE `venditore` (
  `Email` char(50) NOT NULL,
  `Password` char(250) NOT NULL,
  `Nome` char(50) NOT NULL,
  `Cognome` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `venditore`
--

INSERT INTO `venditore` (`Email`, `Password`, `Nome`, `Cognome`) VALUES
('benedetta.bottari@tecweb.it', '$2y$10$kcgh0DYNdNvZtfz8jKaLLOVfHFe1ZZL4Zgbg1Ncbp4XJSXmiwWZ8C', 'Benedetta', 'Bottari'),
('marco.ragazzini@tecweb.it', '$2y$10$RYA.nTTDz/6kDc7iKF9GmOxJQd2bZMML6alBB0ct2BQL6oIjTYtBi', 'Marco', 'Ragazzini');

-- --------------------------------------------------------

--
-- Struttura della tabella `venditore_notifica`
--

CREATE TABLE `venditore_notifica` (
  `Id_VN` int(11) NOT NULL,
  `Notifica` char(50) NOT NULL,
  `Venditore` char(50) NOT NULL,
  `Data` datetime NOT NULL,
  `Descrizione` char(250) NOT NULL,
  `Visualizzato` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `wishlist`
--

CREATE TABLE `wishlist` (
  `Prodotto` char(50) NOT NULL,
  `Utente` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `wishlist`
--

INSERT INTO `wishlist` (`Prodotto`, `Utente`) VALUES
('Café de olla', 'marco.ragazzini@gmail.it'),
('Menù austria', 'marco.ragazzini@gmail.it');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `carrello`
--
ALTER TABLE `carrello`
  ADD PRIMARY KEY (`Id_Carrello`),
  ADD KEY `EQU_CARRE_UTENT_FK` (`Utente`);

--
-- Indici per le tabelle `carta`
--
ALTER TABLE `carta`
  ADD PRIMARY KEY (`Numero`);

--
-- Indici per le tabelle `carta_utente`
--
ALTER TABLE `carta_utente`
  ADD PRIMARY KEY (`Utente`,`Carta`),
  ADD KEY `REF_Carta_CARTA_FK` (`Carta`);

--
-- Indici per le tabelle `indirizzo`
--
ALTER TABLE `indirizzo`
  ADD PRIMARY KEY (`Id_Indirizzo`);

--
-- Indici per le tabelle `notifica`
--
ALTER TABLE `notifica`
  ADD PRIMARY KEY (`Nome`);

--
-- Indici per le tabelle `offerta`
--
ALTER TABLE `offerta`
  ADD PRIMARY KEY (`Codice`),
  ADD KEY `REF_OFFER_VENDI_FK` (`Venditore`);

--
-- Indici per le tabelle `offerta_utente`
--
ALTER TABLE `offerta_utente`
  ADD PRIMARY KEY (`Offerta`,`Utente`),
  ADD KEY `REF_Offer_Utente_FK` (`Utente`);

--
-- Indici per le tabelle `ordine`
--
ALTER TABLE `ordine`
  ADD PRIMARY KEY (`Carrello`),
  ADD KEY `REF_Ordine_Off` (`Offerta`),
  ADD KEY `REF_Ordine_Crt` (`Carta`),
  ADD KEY `REF_Offer_IND_FK` (`Indirizzo`);

--
-- Indici per le tabelle `prodotto`
--
ALTER TABLE `prodotto`
  ADD PRIMARY KEY (`Nome`),
  ADD KEY `REF_PRODO_VENDI_FK` (`Venditore`);

--
-- Indici per le tabelle `prod_carr`
--
ALTER TABLE `prod_carr`
  ADD PRIMARY KEY (`Carrello`,`Prodotto`),
  ADD KEY `REF_PROD__PRODO_FK` (`Prodotto`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`Email`);

--
-- Indici per le tabelle `utente_indirizzo`
--
ALTER TABLE `utente_indirizzo`
  ADD PRIMARY KEY (`Indirizzo`,`Utente`),
  ADD KEY `EQU_Utent_UTENT_FK` (`Utente`);

--
-- Indici per le tabelle `utente_notifica`
--
ALTER TABLE `utente_notifica`
  ADD PRIMARY KEY (`Id_UN`),
  ADD KEY `EQU_Utent_UTENT_NOT_FK` (`Utente`),
  ADD KEY `REF_Utent_NOT` (`Notifica`);

--
-- Indici per le tabelle `venditore`
--
ALTER TABLE `venditore`
  ADD PRIMARY KEY (`Email`);

--
-- Indici per le tabelle `venditore_notifica`
--
ALTER TABLE `venditore_notifica`
  ADD PRIMARY KEY (`Id_VN`),
  ADD KEY `EQU_Utent_UTENT_NOT_FK` (`Venditore`),
  ADD KEY `REF_Utent_NOT` (`Notifica`);

--
-- Indici per le tabelle `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`Prodotto`,`Utente`),
  ADD KEY `REF_Wishl_WISHL` (`Utente`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `carrello`
--
ALTER TABLE `carrello`
  MODIFY `Id_Carrello` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `indirizzo`
--
ALTER TABLE `indirizzo`
  MODIFY `Id_Indirizzo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `utente_notifica`
--
ALTER TABLE `utente_notifica`
  MODIFY `Id_UN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT per la tabella `venditore_notifica`
--
ALTER TABLE `venditore_notifica`
  MODIFY `Id_VN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `carrello`
--
ALTER TABLE `carrello`
  ADD CONSTRAINT `EQU_CARRE_UTENT_FK` FOREIGN KEY (`Utente`) REFERENCES `utente` (`Email`);

--
-- Limiti per la tabella `carta_utente`
--
ALTER TABLE `carta_utente`
  ADD CONSTRAINT `REF_Carta_CARTA_FK` FOREIGN KEY (`Carta`) REFERENCES `carta` (`Numero`),
  ADD CONSTRAINT `REF_Carta_UTENT` FOREIGN KEY (`Utente`) REFERENCES `utente` (`Email`);

--
-- Limiti per la tabella `offerta`
--
ALTER TABLE `offerta`
  ADD CONSTRAINT `REF_OFFER_VENDI_FK` FOREIGN KEY (`Venditore`) REFERENCES `venditore` (`Email`);

--
-- Limiti per la tabella `offerta_utente`
--
ALTER TABLE `offerta_utente`
  ADD CONSTRAINT `REF_Offer_Off` FOREIGN KEY (`Offerta`) REFERENCES `offerta` (`Codice`),
  ADD CONSTRAINT `REF_Offer_Utente_FK` FOREIGN KEY (`Utente`) REFERENCES `utente` (`Email`);

--
-- Limiti per la tabella `ordine`
--
ALTER TABLE `ordine`
  ADD CONSTRAINT `REF_Offer_IND_FK` FOREIGN KEY (`Indirizzo`) REFERENCES `indirizzo` (`Id_Indirizzo`),
  ADD CONSTRAINT `REF_Ordine_Crl` FOREIGN KEY (`Carrello`) REFERENCES `carrello` (`Id_Carrello`),
  ADD CONSTRAINT `REF_Ordine_Crt` FOREIGN KEY (`Carta`) REFERENCES `carta` (`Numero`),
  ADD CONSTRAINT `REF_Ordine_Off` FOREIGN KEY (`Offerta`) REFERENCES `offerta` (`Codice`);

--
-- Limiti per la tabella `prodotto`
--
ALTER TABLE `prodotto`
  ADD CONSTRAINT `REF_PRODO_VENDI_FK` FOREIGN KEY (`Venditore`) REFERENCES `venditore` (`Email`);

--
-- Limiti per la tabella `prod_carr`
--
ALTER TABLE `prod_carr`
  ADD CONSTRAINT `REF_PROD__CARRE_FK` FOREIGN KEY (`Carrello`) REFERENCES `carrello` (`Id_Carrello`),
  ADD CONSTRAINT `REF_PROD__PRODO_FK` FOREIGN KEY (`Prodotto`) REFERENCES `prodotto` (`Nome`);

--
-- Limiti per la tabella `utente_indirizzo`
--
ALTER TABLE `utente_indirizzo`
  ADD CONSTRAINT `EQU_Utent_UTENT_FK` FOREIGN KEY (`Utente`) REFERENCES `utente` (`Email`),
  ADD CONSTRAINT `REF_Utent_INDIR` FOREIGN KEY (`Indirizzo`) REFERENCES `indirizzo` (`Id_Indirizzo`);

--
-- Limiti per la tabella `utente_notifica`
--
ALTER TABLE `utente_notifica`
  ADD CONSTRAINT `EQU_Utent_UTENT_NOT_FK` FOREIGN KEY (`Utente`) REFERENCES `utente` (`Email`),
  ADD CONSTRAINT `REF_Utent_NOT` FOREIGN KEY (`Notifica`) REFERENCES `notifica` (`Nome`);

--
-- Limiti per la tabella `venditore_notifica`
--
ALTER TABLE `venditore_notifica`
  ADD CONSTRAINT `EQU_vend_VEND_NOT_FK` FOREIGN KEY (`Venditore`) REFERENCES `venditore` (`Email`),
  ADD CONSTRAINT `REF_vend_NOT` FOREIGN KEY (`Notifica`) REFERENCES `notifica` (`Nome`);

--
-- Limiti per la tabella `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `REF_Wishl_Prd` FOREIGN KEY (`Utente`) REFERENCES `utente` (`Email`),
  ADD CONSTRAINT `REF_Wishl_Ut_FK` FOREIGN KEY (`Prodotto`) REFERENCES `prodotto` (`Nome`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
